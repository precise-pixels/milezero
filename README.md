# Mile Zero

Mile Zero is an opinionated, responsive theme and environment for bootstrapping front-end development.
This is a _mostly_ blank canvas on which to build your own designs.

The methodologies employed are from our experience in developing small to medium websites
over the past few years. Thanks goes out to all the hard work and research done by the countless individuals
who make the web design and development community a pleasure to be a part of.

## What Mile Zero gives you:

* Normalized CSS styles via [Normalize.css](http://necolas.github.io/normalize.css/)
* Sass generated stylesheets
* [Susy](http://susy.oddbird.net) for a responsive grid system
* [Modernizr.js](http://modernizr.com/)
* [Gulp](http://gulpjs.com)-based workflow including CSS/JS linting, Sass compilation, and livereload
* Front-end dependancies managed with [Bower](http://bower.io)

## Dependancies

### Susy

We use [Susy 2.1.~](http://susy.oddbird.net) to do the math on our grid. Susy is a Ruby gem and must be installed
with `gem install susy` before you can run the styles Gulp task.

### Gulp

Install [Gulp](http://gulpjs.com), then run `npm install` to install all the required Gulp dependancies.

### Bower

All front-end package dependancies are managed with [Bower](http://bower.io).

You can install Bower globally by running `npm install -g bower`. Follow this command up with `bower install`
to fetch the the front-end dependancies.

## Browser Compatibility

We aim to support modern browsers with this package, inclusive of the following:

* Google Chrome 28+
* Mozilla Firefox 27+
* Safari 5+
* Opera 15+
* Internet Explorer 9+
* Safari for iOS 5+
* Google Chrome for Android 28+

## License

The MIT License (MIT)

Copyright (c) 2014 Centerdrive Graphic and Web Corp.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
