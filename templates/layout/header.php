<!doctype html>
<!--[if IE 8 ]><html class="ie ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Mile Zero</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Spiders must use meta description -->
        <meta name="robots" content="noodp, noydir">

        <!-- No Google Translate toolbar -->
        <meta name="google" content="notranslate">

        <!-- Viewport and mobile -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0, minimal-ui">
        <meta name="HandheldFriendly" content="true">
        <meta name="MobileOptimized" content="320">
        <meta http-equiv="cleartype" content="on">

        <!-- iOS web app -->
        <!-- Note: more options here: https://gist.github.com/tfausak/2222823 -->

        <!--
    	<meta name="apple-mobile-web-app-capable" content="yes">
    	<meta name="apple-mobile-web-app-title" content="__PAGE_TITLE__">
    	<meta name="apple-mobile-web-app-status-bar-style" content="black">
        -->

        <!-- Icons -->
        <!-- Note: One touch icon works but is a tad heavier (http://mathiasbynens.be/notes/touch-icons) -->

        <!--
        <link rel="apple-touch-iconprecomposed" sizes="152x152" href="assets/images/icons/apple-touch-icon-152x152.png">
    	<link rel="icon" href="assets/images/icons/favicon.png">
        <meta name="msapplication-TileImage" content="assets/images/icons/wp-tile-icon-144x144.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
    	-->

        <link rel="stylesheet" href="dist/css/main.css" />

        <!-- This is an un-minified, complete version of Modernizr.
                 Before you move to production, you should generate a custom build that only has the detects you need. -->
        <script src="assets/vendor/modernizr/modernizr.js"></script>

        <!--[if lt IE 9]>-->
        <!--<script src="assets/js/ie.head.min.js"></script>
        <link rel="stylesheet" href="assets/css/ie.min.css">-->
        <!--<![endif]-->

        <!-- Application-specific meta tags -->
        <!-- Windows 8 -->
        <meta name="application-name" content="" />
        <meta name="msapplication-TileColor" content="" />
        <meta name="msapplication-TileImage" content="" />
        <!-- Twitter -->
        <meta name="twitter:card" content="">
        <meta name="twitter:site" content="">
        <meta name="twitter:title" content="">
        <meta name="twitter:description" content="">
        <meta name="twitter:url" content="">
        <!-- Facebook -->
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="" />

    </head>

    <body>
