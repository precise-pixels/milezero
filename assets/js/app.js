// Browser detection for when you get desperate.
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }

// remap jQuery to $
(function(window, $){

'use strict';
// Document -------------------------------------------------------------------

$(document).ready(function (){

    // VARS -------------------------------------------------------------------
    //

    var
        $window = $(window),
        $body = $('body')
        ;

    // ACTIONS ----------------------------------------------------------------
    //

    // $elem.on('something', doSomething);

    //
    // FUNCTIONS --------------------------------------------------------------
    //

    // function doSomething () {}

});

})(window, window.jQuery);
