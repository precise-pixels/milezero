<?php include __DIR__ . '/templates/layout/header.php'; ?>

	<div class="container">
	<!-- not needed? up to you: http://camendesign.com/code/developpeurs_sans_frontieres -->

		<header class="header" role="header">
				<h1><a href="/" title="Site Title" rel="home">Site Title</a></h1>
		</header>

		<section class="content">

	 		<!-- Content goes here -->

        </section>

		<footer class="footer" role="contentinfo">
    		<small>&copy; Site Title. All Rights Reserved.</small>
		</footer>

	</div>

<?php include __DIR__ . '/templates/layout/footer.php'; ?>
