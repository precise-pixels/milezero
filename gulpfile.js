'use strict';

var
    chalk           = require('chalk'),

    gulp            = require('gulp'),
    clean           = require('gulp-clean'),
    concat          = require('gulp-concat'),
    deporder        = require('gulp-deporder'),
    gulpif          = require('gulp-if'),
    gutil           = require('gulp-util'),
    imagemin        = require('gulp-imagemin'),
    jshint          = require('gulp-jshint'),
    newer           = require('gulp-newer'),
    livereload      = require('gulp-livereload'),
    notify          = require('gulp-notify'),
    plumber         = require('gulp-plumber'),
    prefix          = require('gulp-autoprefixer'),
    uglify          = require('gulp-uglify'),
    rename          = require('gulp-rename'),
    replace         = require('gulp-replace'),
    sass            = require('gulp-ruby-sass'),
    sequence        = require('run-sequence'),
    stylish         = require('jshint-stylish'),

    flags           = require('minimist')(process.argv.slice(2)),
    isProduction    = flags.production || flags.prod || false,
    watching        = flags.watch || false
    ;


// BUILD ------------------------------------------------------------------------
//

gulp.task('build', function() {

    console.log(chalk.green('Building ' + (flags.production ? 'production' : 'dev') + ' version...'));

    if (flags.watch) {
        sequence(
            'clean',
            [
                'sass-main',
                'sass-ie',
                'scripts-main',
                'scripts-ie',
                'images',
                'other'
            ],
            'watch',
            function() {
                console.log(chalk.green('Going into watch mode...'))
            }
        )
    } else {
        sequence(
            'clean',
            [
                'sass-main',
                'sass-ie',
                'scripts-main',
                'scripts-ie',
                'images',
                'other'
            ],
            function() {
                console.log(chalk.green('✔ All done!'))
            }
        )
    }

});


// SASS ------------------------------------------------------------------------
//

// Note: Once libsass fixed the @extend bug, we will switch
// to that implementation rather than the Ruby one (which is slower).
// https://github.com/hcatlin/libsass/issues/146
gulp.task('sass-main', function() {
    return gulp.src([
            'assets/sass/*.{scss, sass, css}',
            '!*ie.{scss, sass, css}'
        ])
        .pipe(plumber())
        .pipe(sass({
            style: isProduction ? 'compressed' : 'nested',
            require: 'susy'
        }))
        .pipe(prefix('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulpif(isProduction, rename({suffix: '.min'})))
        .pipe(gulp.dest('dist/css'))
    ;
});

gulp.task('sass-ie', function() {
    return gulp.src([
            'assets/sass/ie.{scss, sass, css}'
        ])
            .pipe(plumber())
            .pipe(sass({
                style: (isProduction ? 'compressed' : 'nested'),
                require: 'susy'
            }))
            .pipe(gulp.dest('dist/css/ie.min.css'))
        ;
});


// SCRIPTS ------------------------------------------------------------------------
//

// JSHint options:	http://www.jshint.com/docs/options/
gulp.task('hint-scripts', function() {
    return gulp.src([
            'assets/js/*.js',
            '!_*.js'
        ])
        .pipe(plumber())
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter(stylish))
    ;
});

gulp.task('scripts-main', ['hint-scripts'], function() {
    return gulp.src([
            'assets/vendor/jquery/jquery*.js',
            (isProduction ? '!' : '') + 'assets/js/libs/dev/*.js',
            'assets/js/libs/*.js',
            'assets/js/*.js'
        ])
        .pipe(plumber())
        .pipe(concat('app.min.js'))
        .pipe(gulpif(isProduction, replace(/(\/\/)?(console\.)?log\((.*?)\);?/g, '')))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest('dist/js'))
    ;
});

gulp.task('scripts-ie', function() {
    gulp.src([
        'assets/js/ie/head/**/*.js'
    ])
        .pipe(plumber())
        .pipe(deporder())
        .pipe(concat('ie.head.min.js'))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest('dist/js'))
    ;

    return gulp.src([
            'assets/js/ie/body/**/*.js'
        ])
        .pipe(plumber())
        .pipe(deporder())
        .pipe(concat('ie.body.min.js'))
        .pipe(gulpif(isProduction, uglify()))
        .pipe(gulp.dest('dist/js'))
    ;
});




// IMAGES ------------------------------------------------------------------------
//

gulp.task('images', function() {

    // Make a copy of the favicon.png, and make a .ico version for IE
    // Move to root of export folder
    gulp.src('assets/img/icons/favicon.png')
        .pipe(plumber())
        .pipe(rename({extname: '.ico'}))
        .pipe(gulp.dest('./'))
    ;

    // Grab all image files, filter out the new ones and copy over
    // In --production mode, optimize them first
    return gulp.src([
            'assets/img/**/*',
            '!_*'
        ])
        .pipe(plumber())
        .pipe(newer('dist/img'))
        .pipe(gulpif(isProduction, imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest('dist/img'))
    ;
});


// OTHER ------------------------------------------------------------------------
//

gulp.task('other', function() {

    // Copy fonts over to the distribution folder
    return gulp.src([
        'assets/**/*.otf',
        'assets/**/*.eot',
        'assets/**/*.svg',
        'assets/**/*.ttf',
        'assets/**/*.woff',
        '!_*'
    ])
        .pipe(plumber())
        .pipe(gulp.dest('dist/fonts'))
    ;
});

// CLEAN ------------------------------------------------------------------------
//

gulp.task('clean', function() {
    return gulp.src([
            'dist/*'
    ], {read: false})
        .pipe(clean())
    ;
});


// WATCH ------------------------------------------------------------------------
gulp.task('watch', function() {

    livereload.listen();

    // Watch .scss files
    gulp.watch('assets/sass/**/*.scss', ['sass-main', 'sass-ie']);

    // Watch .js files
    gulp.watch('assets/js/**/*.js', ['scripts-main', 'scripts-ie']);

    // Watch image files
    gulp.watch('assets/img/**/*', ['images']);

    // Notify Livereload of changes
    gulp.watch('dist/**/*').on('change', livereload.changed);

});


// DEFAULT ----------------------------------------------------------------------
gulp.task('default', function() {
    gulp.start('build');
});
